# Dune 2000 speedrun guide

## instalation
To install the game I would advice the gruntsmod edition. This can be downloaded from there website: https://gruntmods.com/dune_2000_gruntmods_edition/

## Missions

### Atraides

#### Mission 1  
For this mission there are 2 maps.  

##### [Mission 1 Top](https://nbviewer.jupyter.org/urls/bitbucket.org/Snips_Mine/dune-2000-guide/raw/29ca5f79f7d1b6a89740df0757255ead64acd6a7/Levels/Ataides/Atraides_mission1_top.ipynb)

##### [Mission 1 Bottom](https://nbviewer.jupyter.org/urls/bitbucket.org/Snips_Mine/dune-2000-guide/raw/ce00b5b220796a1306d6c94f8a6a90909612c989/Levels/Atraides/Atraides_mission1_bottom.ipynb)

#### Mission 2
For this mission there are 2 maps.

##### [Mission 2 Top](https://nbviewer.jupyter.org/urls/bitbucket.org/Snips_Mine/dune-2000-guide/raw/ce00b5b220796a1306d6c94f8a6a90909612c989/Levels/Atraides/Atreides_mission2_top.ipynb)